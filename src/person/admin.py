from django.contrib import admin
from .models import Person


class PersonAdmin(admin.ModelAdmin):
    'Параметры консоли администратора для модели Person.'

admin.site.register(Person, PersonAdmin)
