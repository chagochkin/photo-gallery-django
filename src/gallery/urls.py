from django.conf.urls import url

from .views import AddPhotoView
from .views import DeletePhotoView
from .views import EditPhotoView
from .views import IndexView


urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    url(r'add$', AddPhotoView.as_view(), name='add-form'),
    url(r'edit/(?P<pk>\d+)$', EditPhotoView.as_view(), name='edit-form'),
    url(r'delete/(?P<pk>\d+)$', DeletePhotoView.as_view(), name='delete-form'),
]
